use std::io::Error;
use std::io::ErrorKind;
use std::path::Path;
use std::path::PathBuf;

use std::fs;

use std::collections::HashMap;
use strfmt::strfmt;

use crate::config;

pub(crate) fn read_dir(dir: &str, pattern: &str) -> Result<Vec<PathBuf>, Error> {
    let mut items: Vec<PathBuf> = Vec::new();

    for entry in fs::read_dir(dir)? {
        let entry = entry?;
        let path = entry.path();
        if path.is_dir() {
            let mut sub_paths = read_dir(path.to_str().unwrap(), pattern)?;
            items.append(&mut sub_paths);
        } else if path.extension().unwrap().to_str().unwrap() == pattern {
            items.push(path);
        }
    }

    Ok(items)
}

pub(crate) fn get_files(pattern: &str, dir: &str) -> Result<Vec<PathBuf>, Error> {
    Ok(read_dir(dir, pattern)?)
}

pub(crate) fn move_file(source: &str, pattern: &str, setup: &config::Setup) -> Result<(), Error> {
    let mut vars = HashMap::new();
    vars.insert("filename".to_string(), source);
    let take_file = strfmt(pattern, &vars).unwrap();
    let mut give_file = take_file.clone();

    let path_loc_start = take_file.find(&setup.input).unwrap();
    let path_loc_end = setup.input.len() + path_loc_start;
    give_file.replace_range(path_loc_start..path_loc_end, &setup.output);

    let folder = Path::new(&give_file).parent().unwrap();
    fs::create_dir_all(folder)?;

    match fs::rename(&take_file, give_file) {
        Ok(()) => (),
        Err(error) => match error.kind() {
            ErrorKind::NotFound => {
                panic!(
                    "output file '{}' not found (check your format/take?)",
                    take_file
                );
            }
            other_error => {
                panic!(
                    "something went mysteriously wrong copying output file '{}': {:?}",
                    take_file, other_error
                );
            }
        },
    };
    Ok(())
}

pub(crate) fn copy_file(source: &str, pattern: &str, setup: &config::Setup) -> Result<(), Error> {
    // deduplicate with above?
    let mut vars = HashMap::new();
    vars.insert("filename".to_string(), source);
    let take_file = strfmt(pattern, &vars).unwrap();
    let mut give_file = take_file.clone();

    let path_loc_start = take_file.find(&setup.input).unwrap();
    let path_loc_end = setup.input.len() + path_loc_start;
    give_file.replace_range(path_loc_start..path_loc_end, &setup.output);

    let folder = Path::new(&give_file).parent().unwrap();
    fs::create_dir_all(folder)?;

    fs::copy(take_file, give_file)?;
    Ok(())
}
