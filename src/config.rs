use std::fs;
use std::io::Error;
use std::io::ErrorKind;
use std::path::PathBuf;

use serde::Deserialize;
use toml::value::Array;
use toml::value::Table;
use toml::value::Value;

#[derive(Deserialize, Debug)]
pub(crate) struct Settings {
    pub setup: Setup,
    pub format: Table,
}

#[derive(Deserialize, Debug)]
pub(crate) struct Setup {
    pub input: String,
    pub output: String,
    pub copy: Option<Array>,
}

// #[derive(Deserialize, Debug)]
// pub(crate) struct Format {
//     pub extension: String,
//     pub command: String,
//     pub take: Array,
// }

pub(crate) fn get_config(config_path: &PathBuf) -> Result<Settings, Error> {
    let config_file = fs::read_to_string(&config_path);

    let config_file = match config_file {
        Ok(file) => file,
        Err(error) => match error.kind() {
            ErrorKind::NotFound => {
                panic!("glintfile '{}' not found", &config_path.to_str().unwrap());
            }
            other_error => {
                panic!(
                    "something went mysteriously wrong reading glintfile '{}': {:?}",
                    &config_path.to_str().unwrap(),
                    other_error
                );
            }
        },
    };

    let settings: Settings = match toml::from_str(&config_file) {
        Ok(settings) => settings,
        Err(_error) => panic!(
            "glintfile '{}' is not valid",
            &config_path.to_str().unwrap()
        ),
    };

    for format in settings.format.values() {
        match check_format(format) {
            Ok(()) => (),
            Err(_error) => {
                panic!(
                    "glintfile '{}' is not valid",
                    &config_path.to_str().unwrap()
                );
            }
        }
    }

    if settings.format.get("clean").is_some() {
        panic!("Can't name format 'clean' (used as inbuilt)");
    }

    Ok(settings)
}

fn check_format(fmt: &Value) -> Result<(), Error> {
    // not the nicest/most accurate but serde dosen't seem to support table<type>
    let fmt = fmt.as_table().unwrap();
    if !(fmt.contains_key("extension") && fmt.contains_key("command") && fmt.contains_key("take")) {
        return Err(Error::new(ErrorKind::Other, "oh no!"));
    }

    Ok(())
}
