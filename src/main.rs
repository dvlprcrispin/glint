// #![allow(dead_code)]
// #![allow(unreachable_code)]
// #![allow(unused_variables)]
// #![allow(unused_imports)]
#![warn(clippy::all)]
#![warn(clippy::pedantic)]

use std::fs;
use std::io::Error;
use std::io::ErrorKind;
use std::panic;
use std::path::PathBuf;
use structopt::StructOpt;
use toml::value::Value;

mod command;
mod config;
mod files;

#[derive(Debug, StructOpt)]
#[structopt(name = "glint", about = "game asset build and collection tool")]
struct Opt {
    /// Runs an individual format (or 'clean') [optional]
    format: Option<String>,

    /// Path to glintfile
    #[structopt(short, long, parse(from_os_str), default_value = "glintfile")]
    glintfile: PathBuf,
}

fn main() -> Result<(), Error> {
    let opt = Opt::from_args();

    panic::set_hook(Box::new(|panic_info| {
        // taken from rust std/panicking
        let msg = match panic_info.payload().downcast_ref::<&'static str>() {
            Some(s) => *s,
            None => match panic_info.payload().downcast_ref::<String>() {
                Some(s) => &s[..],
                None => "Box<Any>",
            },
        };
        println!("{}", msg);
    }));

    let config = config::get_config(&opt.glintfile)?;

    if fs::metadata(&config.setup.input).is_err() {
        panic!("input folder '{}' does not exist", config.setup.input);
    }

    let steps = config.format.len();

    if let Some(target_format) = opt.format {
        if let Some(format) = config.format.get(&target_format) {
            process_format(format, &config)?;
        } else if target_format == "clean" {
            match fs::remove_dir_all(&config.setup.output) {
                Ok(_) => (),
                Err(error) => {
                    match error.kind() {
                        ErrorKind::NotFound => {
                            panic!("output folder '{}' already clean", config.setup.output);
                        }
                        other_error => {
                            panic!("something went mysteriously wrong cleaning output folder '{}': {:?}", config.setup.output, other_error);
                        }
                    }
                }
            }
        } else {
            panic!("No format '{}' exists", target_format);
        }
    } else {
        if let Some(copys) = &config.setup.copy {
            println!("[0/{}] inital copy", steps);
            for copy in copys {
                let pattern = copy.as_str().unwrap();
                let extension = pattern.replace("{filename}.", "");
                let files_to_process = files::get_files(&extension, &config.setup.input)?;
                for mut file in files_to_process {
                    file.set_extension("");
                    files::copy_file(file.to_str().unwrap(), pattern, &config.setup)?;
                }
            }
        }

        for (i, (name, format)) in config.format.iter().enumerate() {
            println!("[{}/{}] {}", i + 1, steps, name);
            process_format(format, &config)?;
        }
    }

    Ok(())
}

fn process_format(format: &Value, config: &config::Settings) -> Result<(), Error> {
    let pattern = format.get("extension").unwrap().as_str().unwrap();
    let files_to_process = files::get_files(pattern, &config.setup.input)?;

    for file in files_to_process {
        let filepath = command::make_filepath(&file)?;
        let command = command::make_command(&filepath, format)?;
        command::run_command(&command)?;

        for take in format.get("take").unwrap().as_array().unwrap() {
            let take = take.as_str().unwrap();
            files::move_file(&filepath, take, &config.setup)?;
        }
    }

    Ok(())
}
