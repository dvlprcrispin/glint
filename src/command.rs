use std::io::Error;
use std::io::ErrorKind;
use std::path::PathBuf;
use std::process::Command;
use toml::value::Value;

use std::collections::HashMap;
use strfmt::strfmt;

pub(crate) fn make_filepath(file: &PathBuf) -> Result<String, Error> {
    //types are fun!
    let mut file_path = file.parent().unwrap().to_path_buf();
    let file_name = file.file_stem().unwrap().to_str().unwrap();
    file_path.push(file_name);

    let file_string = file_path.into_os_string().into_string().unwrap();

    Ok(file_string)
}

pub(crate) fn make_command(file_string: &str, file_format: &Value) -> Result<String, Error> {
    let cmd_template = file_format.get("command").unwrap().as_str().unwrap();
    let mut vars = HashMap::new();
    vars.insert("filename".to_string(), file_string);
    let command = strfmt(cmd_template, &vars).unwrap();
    Ok(command)
}

pub(crate) fn run_command(command: &str) -> Result<(), Error> {
    let command_vec: Vec<&str> = command.split(' ').collect();
    let (exe, args) = command_vec.split_first().unwrap();
    let mut process = Command::new(exe);
    for arg in args {
        process.arg(arg);
    }

    match process.output() {
        Ok(output) => output,
        Err(error) => match error.kind() {
            ErrorKind::NotFound => {
                panic!("program '{}' could not be found", exe);
            }
            other_error => {
                panic!(
                    "something went mysteriously wrong running the command '{}': {:?}",
                    command, other_error
                );
            }
        },
    };
    Ok(())
}
